package example;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

import static java.util.concurrent.TimeUnit.SECONDS;

public class Test {

    public static void main(final String[] args) throws UnknownHostException {
        // connect to the local database server
        MongoClient mongoClient = new MongoClient();
        

        // get handle to "mydb" (Data base)
        DB db = mongoClient.getDB("mydb");

        //String myUsername = "amazon" ;
        //char[] password = {'1','2','3'} ;
		//boolean auth = db.authenticate(myUsername, password) ;

        // get a list of the collections in this database and print them out
        Set<String> collectionNames = db.getCollectionNames();
        for (final String s : collectionNames) {
            System.out.println(s);
        }

        // get a collection object to work with
        DBCollection testCollection = db.getCollection("testCollection");

        // drop all the data in it
        testCollection.drop();

        ///////////////////////////
        // make a document and insert it
        for (int i=0; i < 100; i++) {
        	testCollection.insert(new BasicDBObject("i", i));
        }
        //length of document
        System.out.println(testCollection.getCount());
        
        //Print all documents
        DBCursor cursor = testCollection.find();
        try {
           while(cursor.hasNext()) {
               System.out.println(cursor.next());
           }
        } finally {
           cursor.close();
        }
        ////////////////////////////////////

        //finding document
        System.out.println("Finding data:");
        BasicDBObject query = new BasicDBObject("i", 71);
        cursor = testCollection.find(query);
        try {
        	   while(cursor.hasNext()) {
        	       System.out.println(cursor.next());
        	   }
        	} finally {
        	   cursor.close();
        	}
        
        // release resources
        mongoClient.close();
    }
    // CHECKSTYLE:ON
}